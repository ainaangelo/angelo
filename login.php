<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <title>BackOffice</title>
  <link type="text/css" href="./css/style.css" rel="stylesheet" />
  <!--[if lte IE 6]>
    <link type="text/css" href="./css/style_ie6.css" rel="stylesheet" />
  <![endif]-->
</head>

<body>
<div id="page">

  <!-- content -->
  <div id="container">
    <div id="logbox">
        	    <div id="logbox-title">ACTUBE> ACCEDER AU BACK OFFICE</div>
        		<div id="logbox_left"></div>
        		<div id="logbox_right">
            		<span >Connecter ici</span>
    	  			<form method="post" action="verification.php" class="logbox-form">
    	  				<p>
    			  			<label for="username">Nom d'utilisateur</label>
    						<input type="text" name="nom" id="username" value="" />
    					</p>
    					<p>
    						<label for="password">Mot de passe</label>
    						<input type="password" name="passe" id="password" value="" />
                        <input type="hidden" name="urlTo" value="" /></br></br>
    					<input type="submit" name="submit" value="Connexion" class="button" />
                        </p>
    				</form>
				</div>
                <div class="clear"></div>
      	</div>
  </div>

</div>
</body>

</html>