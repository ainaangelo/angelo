<!DOCTYPE HTML>
<!--
	Full Motion by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<?php require ('fonction.php') ?>
  <?php
$idd=$_GET["jk"];
 $donnees = findbyid($idd);
  $titre = findbyid($idd);?>
<html>
	<head>
		<title>ACTUBE</title>
		<meta charset="utf-8" />
		<meta name="description" content="video celebre sur youtube et les videos qui fait le buzz sur internet" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body id="top">

			<!-- Banner -->
			<!--
				To use a video as your background, set data-video to the name of your video without
				its extension (eg. images/banner). Your video must be available in both .mp4 and .webm
				formats to work correctly.
			-->
				<section id="banner" data-video="images/banner">
					<div class="inner">
						<header>
							<h1>A PROPOS</h1>
							<?php foreach ($titre AS $title){?>
							<h2> C'est  <?php echo $title['categorie'] ?> </br> <?php echo $title['description'] ?> </h2>
							<?php }?>
						</header>
						<a href="#main" class="more">Learn More</a>
					</div>
				</section>

			<!-- Main -->
				<div id="main">
					<div class="inner">

					<!-- Boxes -->
						<div class="thumbnails">
						
							<?php foreach ($donnees AS $donnee){?>
							
							<div class="box">
								<a href="<?php echo $donnee['lien'] ?>" class="image fit">  <img src="images/<?php echo $donnee["image"]; ?>" width="90"> </a>
								<div class="inner">
									<h3><?php echo $donnee['categorie'] ?></h3>
									<p><?php echo $donnee['description'] ?></p>
									<a href="<?php echo $donnee['lien'] ?>" class="button fit" data-poptrox="youtube,800x400">Regarder</a>
									<a href="actube.html" class="button fit" data-poptrox="youtube,800x400">Accueil</a>
								</div>								
							</div>
							<?php }?>
						</div>						
					</div>
				</div>
				
				
				

			<!-- Footer -->
				<footer id="footer">
					<div class="inner">
					
						<h2>Actube c'est un site qui poste plusieur categorie de video</h2>
						<p>Contactez nous. </p>

						<ul class="icons">							
							<li><a href="https://www.facebook.com/angelo.nelson.188" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							<li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
						</ul>
						<p class="copyright">&copy; 2018. actube:  Connexion: <a href="login.php">admin</a></p>
					</div>
				</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>