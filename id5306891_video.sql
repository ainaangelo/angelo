-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 06 avr. 2018 à 14:25
-- Version du serveur :  10.1.31-MariaDB
-- Version de PHP :  7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `id5306891_video`
--

-- --------------------------------------------------------

--
-- Structure de la table `element`
--

CREATE TABLE `element` (
  `id` tinyint(20) NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lien` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categorie` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `element`
--

INSERT INTO `element` (`id`, `image`, `lien`, `categorie`, `description`) VALUES
(1, 'pic01.jpg', 'https://www.youtube.com/watch?v=imsGxKyiuD4', 'buzz', 'On parle ici du salon de Geneve avec la voiture qui fait le buzz'),
(2, 'pic02.jpg', 'https://www.youtube.com/watch?v=u5d8udR0Wlw', 'buzz', 'une fille qui danse sur son skate'),
(3, 'image1.jpg', 'https://www.youtube.com/watch?v=QKLfkrd9byY', 'buzz', 'enfant qui fait le buzz sur le net'),
(4, 'image2.png', ' https://www.youtube.com/watch?v=OGNBS9-CiWs', 'buzz', 'essayer de ne pas rire'),
(5, 'image4.png', 'https://www.youtube.com/watch?v=PZEHtseIZ-o', 'clip', ' KIZOMBA 2016 - NOUVEAU CLIP - JACK & SARA'),
(6, 'image3.jpg', 'https://www.youtube.com/watch?v=x9WhnXUb964', 'buzz', 'Les plus gros buzz du foot en compilation 2015');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` tinyint(20) DEFAULT NULL,
  `nom` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motdepasse` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `motdepasse`) VALUES
(1, 'angelo', 'angelo');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `element`
--
ALTER TABLE `element`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
