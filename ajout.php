<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	
<?php require ('fonction.php') ?>
 
  
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <title>BackOffice</title>
  <link type="text/css" href="./css/style.css" rel="stylesheet" />
  <!--[if lte IE 6]>
    <link type="text/css" href="./css/style_ie6.css" rel="stylesheet" />
  <![endif]-->
  <script type="text/javascript" src="./js/jquery-1.2.6.min.js"></script>
  <script type="text/javascript" src="./js/script.js"></script>
</head>

<body>
<div id="page">

  <!-- header -->
  <div id="header">
      <div id="logo"><h1><a href="#" title="Your site name">Actube</a></h1></div>
      <div id="quicklink"><a href="#" title="Admin profile">Admin</a>  <a href="#" title="Help"></a>  <a href="#" title=""></a></div>
  </div>
  <!-- end header -->


  <!-- main menu -->
  <div id="mainmenu">
      <ul>
          <li><a href="#" title=""></a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
          <li><a href="#" title="" class="active">Backoffice</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
          <li><a href="#" title=""></a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
          <li><a href="#" title=""></a></li>
      </ul>
  </div>
  <!-- end mainmenu -->

    <div id="submenu">
        <ul>
            <li><a href="#" title=""></a></li>
            <li><a href="#" title="" class="active"> Liste</a></li>
            <li><a href="#" title=""></a></li>
            <li><a href="#" title=""> </a></li>
        </ul>
        <div class="clear"></div>
    </div>

  <!-- content -->
      <div id="content">

                <!-- block Fade in/out Message box -->

                <h3><span class="title">Pour admin seulement</span><span class="underlined">&nbsp;</span></h3>
               
                <div class="msgbox" id="msgbox1">
                    <div class="icon"><img src="img/icons/alert.gif" alt="" title="" /></div>
                   
                    <div class="close"><a href="#" id="close_msgbox" title="Close message box"><img src="img/icons/icon_minus.gif" alt="" title="" /></a></div>
                    <div class="clear"></div>
                </div>
                <!-- end block Fade in/out Message box -->

                <!-- block example table -->

                <h3><span class="title">Liste des elements</span><span class="underlined">&nbsp;</span></h3>
                 <form  enctype="multipart/form-data" class="customform" action="fonctionajoute.php" id="ContactForm" method="post">
                <div class="tablebox">
                  <table>
                      <thead>
                          <tr>
							<th>id</th>
                            <th>Image</th>
                            <th>Lien</th>
							<th>Categorie</th>
                            <th>Description</th>
                            
                            <th class="action">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr class="row0">
						   <?php
								$do1nnees = idmax();
								foreach ($do1nnees AS $do1nnee){									
									$limit =  $do1nnee['max(id)'] + 1;
								}
							?>
						   <td><input type="number"  readonly="readeonly"  value="<?php echo $limit; ?>" class="input" name="id" ></td>
						  
                            <td>
							  <input type="file" name="images" />
							</td>
							
							
                            <td><input type="text"  placeholder="lien" class="input" name="lien" ></td>
                             <td><input type="text"  placeholder="categori" class="input" name="categorie" ></td>
                            <td>  <textarea name="description"  placeholder="description" tabindex="80"></textarea></td>
							
                            <td><button class="color-btn" type="submit">Ajouter</button></td>  
                          </tr>
                         
                      </tbody>
                  </table>                 
                </div>
                </form>

      </div>
      <!-- end div content -->

  <!-- Footer -->
  <div id="footer">
      <ul>
          <li>&copy;2008 <a href="#" title="">Actube</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
          <li>Powered by <a href="#" title="">Aidan</a></li>
      </ul>
  </div>

</div>
<!-- end div page -->
</body>

</html>