create database buzz;
https://www.youtube.com/watch?v=u5d8udR0Wlw
use buzz;

create table element (
	id TINYINT(20),
	image VARCHAR(50),
	lien VARCHAR(50),
	categorie VARCHAR(20),
	description VARCHAR(200),
	PRIMARY KEY (id)
)engine=InnoDB;

create table utilisateur (	
	id TINYINT(20),	
	nom VARCHAR(20),
	motdepasse VARCHAR(20)	
)engine=InnoDB;

INSERT INTO utilisateur VALUES(1,'angelo','angelo');

INSERT INTO element VALUES('1','pic01.jpg', 'https://www.youtube.com/watch?v=imsGxKyiuD4' , 'buzz','On parle ici du salon de Geneve avec la voiture qui fait le buzz');
INSERT INTO element VALUES('2','pic02.jpg', 'https://www.youtube.com/watch?v=u5d8udR0Wlw' , 'buzz','une fille qui danse sur son skate');
INSERT INTO element VALUES('3','image1.jpg', 'https://www.youtube.com/watch?v=QKLfkrd9byY' , 'buzz','enfant qui fait le buzz sur le net');
INSERT INTO element VALUES('4','image2.png', ' https://www.youtube.com/watch?v=OGNBS9-CiWs' , 'buzz','essayer de ne pas rire');
INSERT INTO element VALUES('5','image4.png', 'https://www.youtube.com/watch?v=PZEHtseIZ-o' , 'clip',' KIZOMBA 2016 - NOUVEAU CLIP - JACK & SARA');
INSERT INTO element VALUES('6','image3.jpg', 'https://www.youtube.com/watch?v=x9WhnXUb964' , 'buzz','Les plus gros buzz du foot en compilation 2015');